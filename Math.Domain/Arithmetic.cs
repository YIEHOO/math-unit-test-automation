﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math.Domain
{
    public class Arithmetic
    {
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }

        public int Addition(int firstNumber, int sencondNumber)
        {
            this.FirstNumber = firstNumber;
            this.SecondNumber = sencondNumber;

            int result = FirstNumber + SecondNumber;

            return result;
        }

        public int Division(int firstNumber, int sencondNumber)
        {
            this.FirstNumber = firstNumber;
            this.SecondNumber = sencondNumber;

            int result = FirstNumber / SecondNumber;

            return result;
        }
    }
}
