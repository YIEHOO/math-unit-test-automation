﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Math.Domain.Test
{
    [TestClass]
    public class ArithmeticTest
    {
        Arithmetic arithmetic = new Arithmetic();

        [TestMethod]
        public void Arithmetic_PositiveNumbers_ReturnsResult()
        {
            Assert.AreEqual(8, arithmetic.Addition(3, 5));
        }

        [TestMethod]
        public void Arithmetic_PositiveNegativeNumber_ReturnsResult()
        {
            Assert.AreEqual(-2, arithmetic.Addition(3, -5));
        }

        [TestMethod]
        public void Arithmetic_NegativePositiveNumber_ReturnsResult()
        {
            Assert.AreEqual(2, arithmetic.Addition(-3, 5));
        }

        [TestMethod]
        public void Arithmetic_NegativeNumbers_ReturnsResult()
        {
            Assert.AreEqual(-8, arithmetic.Addition(-3, -5));
        }

        [TestMethod]
        [ExpectedException(typeof(System.DivideByZeroException))]
        public void Arithmetic_ZeroNumbers_ReturnsResult()
        {
            arithmetic.Division(5, 0);
        }
    }
}
